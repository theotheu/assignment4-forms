---
Hand-out Document can be distributed *after* the workshop.

---


# Classroom exercises

- Find all documents from collection groups
  - `db.groups.find()`
  - [Read more](http://docs.mongodb.org/manual/reference/method/db.collection.find/)
- Find 1 document from collection groups
  - With a specific _id: `db.groups.find({_id: ObjectId("541d9784f744e284c8955352")})`
  - With a specific attribute: `db.groups.find({name: "HR"})`
- Find some documents from collection groups
  - With a specific string `db.groups.find({description: /relations/i})`
- Create 1 document for collection groups
  - ```javascript
   db.groups.insert(
      {
        description: "new",
        name: "new",
        privileges:[],
        users:[]
      })```

  - You can add random extra document attributes
  ```javascript
   db.groups.insert(
      {
        description: "new",
        name: "new",
        privileges:[],
        users:[],
        myAttribute: "something new"
      }) ```

  - [Read more](http://docs.mongodb.org/manual/reference/method/db.collection.insert/)

- Update 1 document for collection groups
  - Update all documents that match the search criteria
  ```javascript
  db.groups.update(
    {
      name:"new"
    },
    {
      name:"new-2",
      description:"new-2"
    })```

  - [Read more](http://docs.mongodb.org/manual/reference/method/db.collection.update/)

- Delete document(s) from collection groups
  - Delete 1 document: `db.groups.remove({name:"new-2"})`
  - Delete all documents: `db.groups.remove({})`
  - [Read more](http://docs.mongodb.org/manual/reference/method/db.collection.remove/)

# Differences SQL / NoSQL
 Note the difficulties to model a relational model for users/groups/privileges into a NoSQL model database like MongoDB
- MongoDb cannot have *intersection tables*.
  - Why not? Because of problems with consistency and sharding
  - Solution? Nest the relations as *subdocuments*.
  - What are consequences of related *subdocuments*? **You** have to do the administration!


# Exercises

## Create nested document
use `$push`

## Retrieve nested document
`db.ikeaProducts.find({"product.items":{$elemMatch:{"partNumber":"00224316"}}});`

## Update nested document

## Delete nested document
use `$pull`